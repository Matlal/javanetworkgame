package lab3.Klient;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Observable;
import java.util.Scanner;

import lab3.Server.Controller;


public class Client_NetworkInterface extends Observer implements Runnable {

	private int portNR, playerNr;
	private String adress;
	private Socket clientSocket;
	private DataInputStream in = null;
	private DataOutputStream out = null;
	private boolean connected=false;
	private String message;
	private boolean joinedGame=false;
	private volatile Thread t;
	//private String grpAdr="239.255.255.250";
	//private int grpPrt=1900;
	//private MulticastSocket MultiSoc;
	private boolean collectedOwnInfo=false;
	private String pos;
	private int observerNR=1;
	private UDP_Klient UDPk;
	//	private byte[] address;
	private int udpPort;
	private InetAddress Ipv6;

	public Client_NetworkInterface(Controler C,String adress2, int portnr1 ) {		
		//connect();
		this.adress=adress2;
		this.portNR=portnr1;
		this.C_subject=C;
		C_subject.attach(this);
//		System.setProperty("java.net.preferIPv6Addresses" , "true");
	}
	public void start() {
		t=new Thread(this);
		t.start();
	}
	public void stopThread() {
		Thread copyT=t;
		if(copyT!=null) {
			t = null;
	        try {
	        	  copyT.interrupt();
				Thread.currentThread().join(400);
			}  catch (InterruptedException e){
	        } 
		}
        
    }
	public boolean isIPv6() throws UnknownHostException{
		for(InetAddress inet : InetAddress.getAllByName(adress)){
			if(inet instanceof Inet6Address){
				this.Ipv6=inet;
				return true;
			}
		}
		return false;
	}

	public boolean connectSuccess() {
		String addresstype="";
		try {
			if(isIPv6()){
				
				clientSocket = new Socket(Ipv6, portNR);
				addresstype="IPv6";
			}
			else{
				clientSocket = new Socket(adress, portNR);
				addresstype="IPv4";
			}
			System.out.println("Trying to connect with local "+addresstype+"- address: "
					+ clientSocket.getLocalSocketAddress()
					+" to foreign address: "+clientSocket.getRemoteSocketAddress().toString());	
			out =new DataOutputStream(clientSocket.getOutputStream());
			in =new DataInputStream(clientSocket.getInputStream());
			connected=clientSocket.isConnected();
			return connected;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public void run(){
		//1. creating a socket to connect to the server
		//			clientSocket = new Socket(adress, portNR);
		//			clientSocket = new Socket(adress, portNR);

		// c = new Controler();
		try {
			 Thread thisThread = Thread.currentThread();
			while(t==thisThread && connected){
				//				System.out.println("waiting for new massage from" +clientSocket.getRemoteSocketAddress().toString() +" through port nr "+ clientSocket.getLocalPort());
				message= in.readUTF();
				System.out.println("Server>" + message);
				C_subject.parseMessage(message);
			}
		}catch(EOFException eof){
			System.err.println("EOFexception! disconnecting...");
			disconnect();
		}catch(UnknownHostException unknownHost){
			System.err.println("You are trying to connect to an unknown host!");
		}catch(SocketException socExc) {
			System.err.println("Lost connection to server");
		}catch(IOException ioException){
			ioException.printStackTrace();
		}finally {
			stopThread();
		    }
	}
	
	private void sendMessage(String msg)
	{
		if(this.connected) {
			try{
				out.writeUTF(msg);
				out.flush();
				System.out.println("--Client>" + msg);
			}
			catch(IOException ioException){
				ioException.printStackTrace();
			}
		}
		
	}

	public void startUDPClient(String multiGrp, String udpPort ) {
		UDPk=new UDP_Klient(C_subject, adress, multiGrp, Integer.parseInt(udpPort));
		Thread udpT=new Thread (UDPk);
		udpT.start();
	}
	//call gui with the message "wait to next game" + time
	/*public void joinRequest() throws Exception{
		InetAddress group = InetAddress.getByName(grpAdr);
		MultiSoc = new MulticastSocket(grpPrt);
		MultiSoc.joinGroup(group);
	}*/

	private void disconnect(){
		joinedGame=false;
		
		if(this.connected) {
			if(!clientSocket.isClosed()) {
				try {
					clientSocket.shutdownInput();
					clientSocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println("Connection with server is now closed.");
			}
			this.connected=false;
		}
		
	}
	
	//	@Override
	//	public void update(Observable arg0, Object arg1) {
	//		// TODO Auto-generated method stub
	//		
	//	}
	//	public void setGameState(GomokuGameState gomokuGameState) {
	//		// TODO Auto-generated method stub
	//		
	//	}

	public void update(Observable o, Object arg) {
		this.C_subject=(Controler) o;
		if(o instanceof Controler){
			String cmd=((String) arg).split(":")[0];

			switch(cmd) {
			case "Disconnect":
				joinedGame=false;
				sendMessage(((String) arg));
				disconnect();
				
				break;
			default:
				sendMessage(((String) arg));
				break;
			}

		}
	}



	public int getIndex() {
		return this.observerNR;
	}
}
