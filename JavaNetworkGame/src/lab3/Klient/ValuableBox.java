package lab3.Klient;

import java.awt.Color;

public class ValuableBox  {

		private boolean selcted=false;
		private int posY,posX;
		private Color C=Color.CYAN; 
		
		public void initVB(int x, int y){
			this.posX=x;
			this.posY=y;
		}
	 	public void setColor(Color newC){
	 		this.C=newC;
	 	}
	 	public Color getColor(){
	 		return this.C;
	 	}
		public int getY(){
			return posY;
		}
		public int getX(){
			return posX;
		}
		public boolean isSelcted(){
			return selcted;
		}
		public void setSelcted(){
			this.selcted=true;
		}
		public String getStrings(){
			String col=C.toString();
			String x=Integer.toString(posX);
			String y=Integer.toString(posY);
			String sel=String.valueOf(selcted);
			
			return x+","+y + "selected="+ sel+", Color:"+col;
		}
}
