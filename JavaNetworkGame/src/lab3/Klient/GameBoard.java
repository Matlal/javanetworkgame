package lab3.Klient;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.*;
import javax.swing.border.*;

@SuppressWarnings("serial")
public class GameBoard extends JPanel  {
	
	private JPanel GamePanel,playerPanel;
	private int size;
	private JPanel[][] Boxes ;
	private Border blackline = BorderFactory.createLineBorder(Color.black);
	private ArrayList<ValuableBox> selectedVBs=new ArrayList<ValuableBox>();
	private ArrayList<Player> players= new ArrayList<Player>();
	private ValuableBox VB;
	private Dimension plpnlDim;
	private Controler C_inst;

	public GameBoard(int Size, Controler C) {
		this.size=Size;
		this.Boxes=new JPanel[size][size];
		this.VB=null;
		plpnlDim=new Dimension(20, 20);
		this.C_inst=C;
		this.setLayout(new BorderLayout(3, 3));
		
	}
	private JPanel getPlayerPnl(final Color c) {
		playerPanel = new JPanel(){
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.setColor(c);
				g.fillOval(5,5,10,10);

			}};
			playerPanel.setPreferredSize(plpnlDim);
		return playerPanel;
	}

	
	public void copyVBs(ArrayList<ValuableBox> vboxes){
		this.selectedVBs=vboxes;
	}
	

	public void initGameBoardGUI() {
		GamePanel=new JPanel(new GridLayout(0, 8));
		//		System.out.println("VB list size : " + this.VBs.size());
		for (int ii = 0; ii < Boxes.length; ii++) {
			for (int jj = 0; jj < Boxes[ii].length; jj++) {
				JPanel Jp = new JPanel();
				Jp.setBorder(blackline);
				Boxes[jj][ii]=Jp;
			}
		}
		for (int ii = 0; ii < this.size; ii++) {
			for (int jj = 0; jj < this.size; jj++) {
				GamePanel.add(Boxes[jj][ii]);
			}
		}
		this.add(GamePanel, BorderLayout.CENTER);
		this.add(new JSeparator(JSeparator.VERTICAL), BorderLayout.EAST);
		this.setPreferredSize(new Dimension(450, 300));
		//		gui.addKeyListener(new InputHandler());
		setKeyBindings();
	}

	public void initColorForPlayers(ArrayList<Player> PList){
		this.players=PList;
		for(Player p: players){
			p.setColor(getRandomColor());
		}
	}

	public Color getRandomColor(){
		Random rand = new Random();
		float r = rand.nextFloat();
		float g = rand.nextFloat();
		float b = rand.nextFloat();
		return new Color(r, g, b);
	}
	
	public void updateGameBoard(ArrayList<Player> PList,ArrayList<ValuableBox> updVBs, ValuableBox newVB, boolean renderPlayer){
		this.players=PList;
		this.VB= newVB;
		this.selectedVBs=updVBs;
		
		for (int ii = 0; ii < size; ii++) {
			for (int jj = 0; jj < size; jj++) {
//				boolean boxBeenRendered = false;
				Boxes[jj][ii].removeAll();
			
				
//				Boxes[jj][ii].revalidate();
				Boxes[jj][ii].getGraphics().setColor(Color.WHITE);
				Boxes[jj][ii].getGraphics().fillRect(0,0,Boxes[jj][ii].getWidth(),Boxes[jj][ii].getHeight());
				/*
				 * This check is to render selected VBs
				 */
				if(this.selectedVBs.size()>0){
					for(ValuableBox vb:selectedVBs){
						if(vb.getX()==jj && vb.getY()==ii){
							
							Boxes[jj][ii].setBackground(vb.getColor());
						}
					}
				}
				/*
				 * This check is to render selected VBs
				 */
			if(this.VB.getX()==jj && this.VB.getY()==ii){
				Boxes[jj][ii].setBackground(this.VB.getColor());
//				boxBeenRendered=true;
			}
			/*
			 * This writes the playr
			 */
			if(renderPlayer) {
				for(Player p : players){
					if(p.getXLoc()==jj && p.getYLoc()==ii){
						final Color c=p.getColor();
						Boxes[jj][ii].add(getPlayerPnl(c));
						
					}
				}
			}
		}
		}
	}
	public JPanel getBox(int x, int y) {
		for (int ii = 0; ii < this.size; ii++) {
			for (int jj = 0; jj < this.size; jj++) {
				if(jj==y && ii == x){
					return Boxes[jj][ii];
				}
			}
		}
		return null;
	}
	public JPanel getGBpnl() {
		return this;
	}
	
	public void setVisibility(boolean beVisible) {
		this.setVisible(beVisible);
	}
	
	private void setKeyBindings() {
	      ActionMap actionMap = getActionMap();
	      int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
	      InputMap inputMap = getInputMap(condition );

	      String vkLeft = "VK_LEFT";
	      String vkRight = "VK_RIGHT";
	      String vkUp= "VK_UP";
	      String vkDown = "VK_DOWN";
	      inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), vkLeft);
	      inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), vkRight);
	      inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), vkUp);
	      inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), vkDown);

	      actionMap.put(vkLeft, new KeyAction(vkLeft));
	      actionMap.put(vkRight, new KeyAction(vkRight));
	      actionMap.put(vkUp, new KeyAction(vkUp));
	      actionMap.put(vkDown, new KeyAction(vkDown));

	   }
	private class KeyAction extends AbstractAction {
	      public KeyAction(String actionCommand) {
	         putValue(ACTION_COMMAND_KEY, actionCommand);
	      }

	      @Override
	      public void actionPerformed(ActionEvent actionEvt) {
//	         System.out.println(actionEvt.getActionCommand() + " pressed");
//	         if(36<actionEvt.getKeyCode()&&e.getKeyCode()<41) {
	    	  
		  			if(C_inst.hasStarted()==true && GamePanel.isVisible()){
		  				switch(actionEvt.getActionCommand()) {
		  				case "VK_LEFT":
		  					C_inst.askServer("MoveReq:West");	
		  					break;
		  				case "VK_UP":
		  					C_inst.askServer("MoveReq:North");	
		  					break;
		  				case "VK_RIGHT":
		  					C_inst.askServer("MoveReq:East");	
		  					break;
		  				case "VK_DOWN":
		  					C_inst.askServer("MoveReq:South");
		  					break;
		  				default:
		  					System.err.println("This keycode should not happen!");
		  					break;
		  				}
		  			}	 
		  		}
//	      }
	   }

}