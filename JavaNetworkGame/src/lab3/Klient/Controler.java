package lab3.Klient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;

import serviceDiscovery.ServiceDescription;


public class Controler extends Observable {
	private List<Observer> observers ;
	private Game gm;
	private int myplayerID, gamesize;
	private boolean joinedGame=false;
	private Client_NetworkInterface CNet1;
	private ServiceListener sListener;
	private ArrayList<String> serviceNames;

	public Controler(int GameSize){
		this.gamesize=GameSize;
		observers = new ArrayList<Observer>();
		this.serviceNames=getServiceNames();
		new client_GUI(this);
	}
	public void endClient() {
		if(CNet1!=null) {
			CNet1.stopThread();
		}
	}
	
	public void askServer(String Req){
		notifyOneObserver(1, Req);
	}
	public boolean tryToConnect(String arg, int port) {
		int TCPportnr;
		if(port == 0){
			TCPportnr=sListener.getService(arg).getPort();
		}else{
			TCPportnr= port;
		}
			String adress= arg; 
			
			/*if(adress.matches("localhost") ||adress.matches("::1") || adress.matches("127.0.0.1")){
				TCPportnr=2005;
			}else{
				TCPportnr=80;
			}*/
//			byte[] addr={127,0,0,1}; 	
			
			CNet1=new Client_NetworkInterface(this,adress, TCPportnr);
			if(CNet1.connectSuccess()){
				
				CNet1.start();
				return true;
			}
			else{
				return false;
			}
	}
	
	public void attach(Observer observer){
		
		observers.add(observer);
	}
	public void deattach(Observer observer){
		observers.remove(observer);
	}
	public void notifyAllObservers(Object obj){
		for (Observer observer : observers) {
			observer.update(this, obj);
		}
	} 
	public void notifyOneObserver( Integer CliNr, Object obj){
		for (Observer observer : observers) {
			if(observer.getIndex()==CliNr){
				observer.update(this, obj);
			}
		}
	}
	
	public ValuableBox getCurrentVB() {
		return gm.getValuableBox();
	}
	public ArrayList<ValuableBox> getSelectedVBs() {
		return gm.getSelectedVBs();
	}
	public ArrayList<Player> getPlayersList() {
		return gm.getPlayerList();
	}
	public int getGameSize(){
		return this.gamesize;
	}
	public boolean hasStarted(){
		if(gm!=null) {
			return gm.hasStarted();
		}
		return false;
	}

	
	public ArrayList<String> getServiceList(){
		return serviceNames;
	}
	public ServiceDescription getServiceInst(String name){
		return sListener.getService(name);
	}
	private ArrayList<String> getServiceNames(){
		sListener= new ServiceListener();
		ArrayList<String> names= new ArrayList<String>();
		for(ServiceDescription name:sListener.getServices()){
			names.add(name.getInstanceName());
			System.out.println("adding service: "+name.getInstanceName()+" to list");
		}
		System.out.println("nr of found services: "+names.size());
		return names;
	}
	

	public void parseMessage(String msg) throws IOException{
		String[] strList= msg.split(":", -1);
		switch(strList[0]){
		case ("Connection successful"):
			break;
		case ("newUDPport"):	
			CNet1.startUDPClient(strList[1],strList[2]);
			break;
		case ("Player ID"):
			this.myplayerID=Integer.parseInt((strList[1]));
			break;
		case ("GameInfo"):
			if(this.joinedGame==false){
				
				this.gm=new Game(this.gamesize);
				notifyOneObserver(2, "newGame");
				this.joinedGame=true;
			}
			String[] players_w_pos= Arrays.copyOfRange(strList, 1, strList.length);
			int i=0;
//			String arr="gameinfo=[";
//			while(i<players_w_pos.length) {
//				arr=arr.concat(players_w_pos[i]);
//				i++;
//				if(i!=players_w_pos.length) {
//					arr=arr+":";
//				}
//			}
//			System.out.println(arr+"]");
			gm.extract_and_check_Players(players_w_pos, this.myplayerID);
			notifyOneObserver(2, "NewPl");
			break;
		case "VBselected":
			int id=Integer.parseInt(strList[1]);
			String[] pos = strList[2].split(",");
			int x=Integer.parseInt(pos[0]);
			int y=Integer.parseInt(pos[1]);
			gm.setVBselected(id, x,y);
			notifyOneObserver(2, "conqVB");
			break;
		case "newValuableBox":
			gm.addNewVB(strList[1]);
			notifyOneObserver(2, "newVB");
			break;
		case "Game start!":
			 gm.start();
			 notifyOneObserver(2, "GameStart");
			 System.out.println("player amount : "+ gm.getPlayerAmount());
			break;
		case "Move":
			String pNr=strList[1];
			String dir=strList[2];
			if(gm.updatePlPos(Integer.parseInt(pNr),dir)){
				notifyOneObserver(2,"move");
			}	
			break;
		case ("Game finished!"):
			this.joinedGame=false;
			notifyOneObserver(2,msg);
			this.gm=null;
			break;
		default : System.out.println("Message can not be handled as a server command!");
			notifyOneObserver(2, strList[0]);
			break;
		}
	}
	

}
