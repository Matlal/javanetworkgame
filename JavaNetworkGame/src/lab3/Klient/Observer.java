package lab3.Klient;

import java.util.Observable;


abstract class Observer {
	   protected Controler C_subject;
	   protected int ObserverNr;
	   public abstract void update(Observable o, Object msg) ;
	   public abstract int getIndex() ;
}
