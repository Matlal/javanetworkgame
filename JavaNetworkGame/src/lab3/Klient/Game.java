package lab3.Klient;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Game {

	private int size;
	private int EMPTY = 0;
	private int grid[][] = new int[size][size];
	private boolean started;
	private ArrayList<Player> players;
	private ArrayList<ValuableBox> selectedVBoxes=new ArrayList<ValuableBox>();
	private ValuableBox currentVB;
	private int myPlayerNr;
	private client_GUI cGUI;
	private ArrayList<String> order;
	
	public Game(int Size) { 
		this.size=Size;
		players= new ArrayList<Player>();
		grid = new int[size][size];
		currentVB=null;
		this.started=false;
	}
	public int getsize() {
		return size;
	}


	public void initGrid(){
		for (int a = 0; a < size; a++) {
			for (int i = 0; i < size; i++) {
				grid[a][i] = EMPTY;
			}
		}
	}
	
	public int getNextPlayerNR(int P_before){
		for(int i=0;i<order.size()-1;i++){
			if(Integer.toString(P_before).matches(order.get(i))){
				if((i+1)==order.size()){
					return Integer.parseInt(order.get(0));
				}else{
					return Integer.parseInt(order.get(i+1));
				}
			}
		}
		return 0;
	}
	
	public ArrayList<String> getOrder(){
		return this.order;
	}
	
	public void start(){
		this.started=true;
	}
	public boolean hasStarted(){
		return this.started;
	}
	public void addPlayer(int playerNr, String pos, boolean b){
		if(!(playerExist(playerNr))){
			int x = Integer.parseInt(pos.split(",")[0]);
			int y = Integer.parseInt(pos.split(",")[1]);
			players.add(new Player(playerNr,x, y,b));
			System.out.println("new player added."+"Amount of players: "+players.size());
		}
	}
	public boolean playerExist(int playerNr){
		for(Player p : players){
			if(p.getId()==playerNr){
				return true;
			}
		}
		return false;
	}
	/*public boolean indexExistInArray(int index, List<Integer> list){
		for(Integer x : list){
			if(x==index){
				return true;
			}
		}
		return false;
	}
*/
	public Player getPlayer(int PlayerNR){
		for(Player p: players){
			if(p.getId()==PlayerNR)
				return p;
		}
		return null;
	}

	public ArrayList<Player> getPlayerList(){
		return players;
	}
	public ValuableBox getValuableBox(){
		return this.currentVB;
	}

	public ArrayList<ValuableBox> getSelectedVBs(){
		return this.selectedVBoxes;
	}
 	public int getPlayerAmount(){
		return players.size();
	}
	/**
	 * To extract coordinates, use culmNr || x = int[0], rowNr || y=int[1]. 
	 * @param pNr
	 * @return
	 */
	public int getPlayerPos(int pNr){
		for (int a = 0; a < size; a++) {
			for (int i = 0; i < size; i++) {
				if(grid[a][i] == pNr){
					return grid[a][i];
				}
			}
		}
		return -1;
	}
	/**
	 * Reads a location of the grid
	 * 
	 * @param x
	 *            The x coordinate
	 * @param y
	 *            The y coordinate
	 * @return the value of the specified location
	 */
	public int getLocation(int x, int y) {
		return grid[y][x];
	}

	/**
	 * Clears the grid of pieces
	 */
	public void clearGrid() {
		for (int a = 0; a < size; a++) {
			for (int i = 0; i < size; i++) {
				grid[a][i] = EMPTY;
			}
		}
	}
	public void extract_and_check_Players( String[] players_w_pos, int myplayerID) {
	for(int i=0; i<players_w_pos.length;i++) {
		int nr=Integer.parseInt(players_w_pos[i]);
		if(nr==myplayerID){
			addPlayer(nr,players_w_pos[i+1], true);
		}else{
			addPlayer(nr,players_w_pos[i+1], false);
		}
		i++;
		}
	}
	public int getMyplayerNR(){
		return this.myPlayerNr;
	}
	public int getPlayerByPos(int y, int x){
		for (int a = 0; a < size; a++) {
			for (int i = 0; i < size; i++) {
				if(y==a && x==i){
					return grid[a][i];
				}
			}
		}
		return -1;
	}
	public void setMyplayerNR(int nr){
		this.myPlayerNr=nr;
	}

	public boolean updatePlPos(int pNr, String dir) {
		Player p=getPlayer(pNr);
		if(p!=null){
			updateGrid(p, false);
			p.updateLoc(dir);
			updateGrid(p, true);
			return true;
		}else{
			return false;
		}
	}
	public void updateGrid(Player p, boolean updated){
		if(updated){
			grid[p.getYLoc()][p.getXLoc()]=p.getId();
		}else{
			grid[p.getYLoc()][p.getXLoc()]=EMPTY;
		}
	}
	
	public void setPlayerList(ArrayList<Player> pls) {
		this.players=pls;
		
	}
	private void changeColOfSelVB(int id) {
		for(Player p:players) {
			if(p.getId()==id) {
				
				currentVB.setColor(p.getColor());
			}
		}
	}
	public void addNewVB(String xy) {
		String[] coords = xy.split(",");
		int x=Integer.parseInt(coords[0]);
		int y=Integer.parseInt(coords[1]);
		this.currentVB= new ValuableBox();
		this.currentVB.initVB(x, y);
		
	}
	public void setVBselected(int id, int x, int y) {
		currentVB.setSelcted();
		changeColOfSelVB(id);
		this.selectedVBoxes.add(currentVB);
	}

}
