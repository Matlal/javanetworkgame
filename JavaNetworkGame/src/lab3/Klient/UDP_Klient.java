package lab3.Klient;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;

public class UDP_Klient extends Observer implements Runnable  {
	
	private MulticastSocket UDP_SendSocket,UDP_RcvSocket;

	private  DatagramPacket packet;
	//	private  ArrayList<String> msgList = new  ArrayList<String>();

	//	private  Vector<String> udprecv=new Vector(); 
	final protected  char[] hexArray = "0123456789ABCDEF".toCharArray();
	private InetAddress IPAddress;
//	private int UDPport; 
	int observerNr=3;
//	private byte[] addr;
	private String address;
	byte[] buf = new byte[256];
	boolean running = true;
	private String group ;
	private int multicastPort;
	private Controler C;


	public UDP_Klient( Controler c, String adress, String grp, int UDPport){
		this.group=grp;
		this.address=adress;
		this.C=c;
		this.multicastPort=UDPport;
	}

	public void run() {
		try{
			IPAddress= InetAddress.getByName(group);
			UDP_RcvSocket = new MulticastSocket(multicastPort);
			UDP_SendSocket = new MulticastSocket();
			UDP_RcvSocket.joinGroup(IPAddress);
			UDP_RcvSocket.setSoTimeout(0); //infinite timeout
			System.out.println("Joined UDP, multicast group at the address: "+IPAddress.getHostAddress()+". Recieving packets at port: "+UDP_RcvSocket.getLocalPort());
//		       UDP_socket.setReuseAddress(true);
			sendUDPMessage("Hello UDP_channel");
			recieve();
		}catch(SocketException sE){
			sE.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	public void recieve(){
		try{
			while(running){
				byte[] buffer = new byte[1024];
				packet = new DatagramPacket(buffer, buffer.length,IPAddress, multicastPort);

				UDP_RcvSocket.receive(packet);
				InetAddress adr= packet.getAddress();
				int port= packet.getPort();
				if(multicastPort!=port){
					multicastPort=port;
				}
				if(this.IPAddress!=adr){
					this.IPAddress=adr;
				}

				// convert msg
				//			 byte[] newbuffer= new byte[packet.getLength()];
				//				System.out.println("new UDP msg read");
				//				for(int i =0;i<packet.getLength();i++){
				//					newbuffer[i]=buffer[i];
				//				}
				//				System.out.println(" UDP from "+packet.getAddress()+" "+bytesToHex(newbuffer)+" "+new java.util.Date());

				String received = new String(packet.getData(), 0, packet.getLength());
				System.out.println("message on UDP-channel> "+received);
				C.parseMessage(received);
			}
			UDP_RcvSocket.leaveGroup(IPAddress);
			UDP_RcvSocket.close();
		}catch(IOException ioE){
			ioE.printStackTrace();
		}
	}

	public String bytesToHex(byte[] bytes) {   //convert byte[] to string
		char[] hexChars = new char[bytes.length * 2];
		for ( int j = 0; j < bytes.length; j++ ) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
	public byte[] hexStringToByteArray(String s) {               //Convert string to byte[]
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
					+ Character.digit(s.charAt(i+1), 16));
		}
		return data;
	}


	public void sendUDPMessage(String msg) {
		System.out.println("--Client to server on UDP channel> "+ msg +" at port: "+UDP_SendSocket.getLocalPort());
		try{ // send request
			buf=msg.getBytes();
			//		IPAddress= InetAddress.getByAddress(addr);

			DatagramPacket packet = new DatagramPacket(buf, buf.length, IPAddress, multicastPort);
			UDP_SendSocket.send(packet);
		}catch(IOException ioE){
			ioE.printStackTrace();
		}
	}

	public void update(Observable o, Object arg) {
		//		this.c=(Controller) o;
		if(arg instanceof String){

			sendUDPMessage(((String) arg));


		}
		if(arg instanceof Integer){

		}

	}
	public int getIndex() {
		// TODO Auto-generated method stub
		return this.observerNr;
	}

	public void setTimeOut() {
		try {
			this.UDP_RcvSocket.setSoTimeout(2000);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


}
