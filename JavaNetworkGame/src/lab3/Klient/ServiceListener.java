package lab3.Klient;

import java.util.ArrayList;
import java.util.Vector;

import serviceDiscovery.ServiceBrowser;
import serviceDiscovery.ServiceBrowserListener;
import serviceDiscovery.ServiceDescription;

public class ServiceListener implements ServiceBrowserListener{
	ServiceBrowser browser;
	ArrayList<ServiceDescription> descriptors;
	public static final String SERVICE_NAME = "JavaGameServer";

	public ServiceListener() {
		descriptors = new ArrayList<ServiceDescription>();

		browser = new ServiceBrowser();
		browser.addServiceBrowserListener(this);
		browser.setServiceName(SERVICE_NAME);
		browser.startListener();
		browser.startLookup();
		System.out.println(
				"Browser started. Will search for 2 secs.");
		try {
			Thread.sleep(2000);
		}
		catch (InterruptedException ie) {
			// ignore
		}
		browser.stopLookup();
		browser.stopListener();

		/* now if the browser found any matches, we'll
		 * print out the complete list, but only connect
		 * to the first one.
		 */
		if (descriptors.size()>0) {

			System.out.println("\n---Game SERVERS---");
			for (ServiceDescription descriptor : descriptors) {
				System.out.println(descriptor.toString());
			}
		}
	}
	public ServiceDescription getService(String name){
		for(ServiceDescription s:descriptors){
			if(s.getInstanceName().matches(name)){
				return s;
			}
		}
		return null;
	}
	
	public ArrayList<ServiceDescription> getServices(){
		return descriptors;
	}
			/* This is an event call-back method the browser uses
			 * when it receives a response. This will only be
			 * called during the two-second execution window.
			 */
			public void serviceReply(ServiceDescription descriptor) {
				int pos = descriptors.indexOf(descriptor);
				if (pos>-1) {
					descriptors.remove(pos);
				}
				descriptors.add(descriptor);
			}
		}
