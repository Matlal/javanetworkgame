package lab3.Klient;

import java.awt.Color;

public class Player {

	private int x,y;
	private int boxCount = 0;
	private int PlayerId;
	private boolean isME;
	private Color c;
	
	/**
	 * 
	 * @param nr	=ID
	 * @param startX	= coordX
	 * @param startY	= coordY
	 * @param ME		= isClient (boolean)?
	 */
 	Player(int nr, int startX, int startY,  boolean ME){
		this.x=startX;
		this.y=startY;
		this.PlayerId=nr;
		this.isME=ME;
	}
 	public void setColor(Color newC){
 		this.c=newC;
 	}
 	public Color getColor(){
 		return this.c;
 	}
 	public int getId(){
		return PlayerId;
	}
	public int getXLoc(){
		return x;
	}
	public int getYLoc(){
		return y;
	}
	public void updateLoc(String dir){
		switch(dir) {
		case "West" :
			x=x-1;
			break; 
		case "East" :
			x=x+1;
			break; 
		case "North" :
			y=y-1;
			break; 
		case "South" :
			y=y+1;
			break;
		}
	}
	public boolean isMe(){
		return isME;
	}
	public int getBoxCount(){
		return boxCount;
	}
	public void incrBoxCount(){
		boxCount++;
	}
	
}
