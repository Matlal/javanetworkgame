package lab3.Klient;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Observable;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import serviceDiscovery.ServiceDescription;

public class client_GUI extends Observer implements Runnable{
	
	private int observerNr = 2;
	private JLabel msgLbl, scndLbl;
	private JToolBar tools = new JToolBar();
	private JComboBox<ArrayList<String>> serviceBox;
	public GameBoard gb;
	private JPanel MessagePanel,endPanel, PInfoP, gamePanel, playersPanel, EastPanel;
	private JButton joinBtn,leaveBtn; 
	private volatile boolean haveJoined=false;
	private boolean firstLoad;
	public String selectedY,movedir;

	public client_GUI(Controler c2){
		this.C_subject=c2;
		
		this.firstLoad=true;
		C_subject.attach(this);
		initGBpnl();
		SwingUtilities.invokeLater(new Runnable() {
	         public void run() {
	        	 writeGUI();
	         }
	      });
	}
	//Private
	private void addMsgPnl(){
		MessagePanel = new JPanel();
		MessagePanel.setLayout(new BoxLayout(MessagePanel, BoxLayout.Y_AXIS));
		Box box = Box.createVerticalBox();
		this.msgLbl=new JLabel("Hello Player! :)");
		msgLbl.setAlignmentX(Component.CENTER_ALIGNMENT);
		box.add(msgLbl);
		MessagePanel.add(box);
		MessagePanel.setBorder(BorderFactory.createTitledBorder("Message"));
	}
	private void addTools(){
		tools.setFloatable(false);
		joinBtn =new JButton("Join Game");
		joinBtn.setVisible(false);
		scndLbl= new JLabel("Choose service:");
		tools.add(scndLbl);
		tools.addSeparator();
		serviceBox=new JComboBox(C_subject.getServiceList().toArray());
		tools.add(serviceBox);
		tools.addSeparator();
		tools.add(joinBtn);
		leaveBtn = new JButton("Close Connection");
		tools.add(leaveBtn); 
		tools.addSeparator();
		/*
		*		strtLbl= new JLabel("Enter URL;");
		*		tools.add(strtLbl);
		*		textField = new JTextField(20);
		*		tools.add(textField);
		*		tools.setBorder(BorderFactory.createTitledBorder("tools"));	
		*/
	}
	private void addPlayersPnl(){
		PInfoP=new JPanel();
		playersPanel=new JPanel();
		EastPanel=new JPanel();
		PInfoP.setLayout(new BoxLayout(PInfoP,BoxLayout.Y_AXIS));
		playersPanel.setLayout(new BoxLayout(playersPanel,BoxLayout.Y_AXIS));
		EastPanel.setLayout(new BorderLayout());
		JLabel pLabel = new JLabel("Players");
		playersPanel.add(pLabel);
		playersPanel.add(PInfoP);
		EastPanel.add(playersPanel, BorderLayout.CENTER);
		EastPanel.add(new JSeparator(JSeparator.VERTICAL), BorderLayout.LINE_END);
		EastPanel.setVisible(false);
		//playersPanel.setBorder(BorderFactory.createTitledBorder("playersPanel"));
	}
	private void updateGamePnl(boolean beVisible){
		gb.setVisibility(beVisible);
		gamePanel =gb.getGBpnl();

	}
	private void setupEndPnl(){
		endPanel= new JPanel();
		endPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		endPanel.setLayout(new BorderLayout());

	}
	private void initGBpnl(){
		this.gb =new GameBoard(this.C_subject.getGameSize(), this.C_subject);
	}
	
	private void writeGUI() {
		
		addMsgPnl();
		addTools();
		updateGamePnl(false);
		addPlayersPnl();
		setupEndPnl();
		 // Listeners:
		
		joinBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				Object eventObj= e.getSource();
				if(eventObj instanceof JButton){
					if(!haveJoined){
						((JButton) eventObj).setText("");
//						leaveBtn.setText("Resign");
						C_subject.askServer("JoinReq");
						msgLbl.setText("Join request has been sent!");
						haveJoined=true;
						updateGamePnl(true);
						EastPanel.setVisible(true);
						endPanel.setFocusable(true);
						endPanel.setFocusTraversalKeysEnabled(false);
						endPanel.requestFocusInWindow();
//						endPanel.setFocusable(true);
//						endPanel.requestFocusInWindow();
					}else{
						//						((JButton) eventObj).setText("Close Connection");
						//						C_subject.endGame();
						//						isConnected=false;
						//						msgLbl.setText("You have left the game!");
						//						gamePanel.setVisible(false);
						//						EastPanel.setVisible(false);
						C_subject.askServer("StartReq");
						joinBtn.setVisible(false); 
						serviceBox.setVisible(false);
					}
				}
			}
		});
		leaveBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				Object eventObj= e.getSource();
				if(eventObj instanceof JButton){
					C_subject.askServer("Disconnect");
	            	C_subject.endClient();
	            	 System.exit(0);
				}
			}
		});
		serviceBox.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				Object eventObj= e.getSource();
				if(eventObj instanceof JComboBox){

					JComboBox j=(JComboBox)eventObj;
					String serviceName=(String)j.getSelectedItem();
					if(serviceName.matches("ConquerCorners_by_Matlal")){
						ServiceDescription serv= C_subject.getServiceInst(serviceName);
						if(C_subject.tryToConnect(serv.getAddress().getHostAddress(), serv.getPort())){
							scndLbl.setText("Connected to "+serv.getAddress().getHostAddress());
							serviceBox.setVisible(false);
							joinBtn.setVisible(true);
						}else{
							System.out.println("trying to connect to address "+ serv.getAddress() + " at portnr "+ serv.getPort());
							msgLbl.setText("Connection to "+serviceName+" failed!");
						}
					}else{
						msgLbl.setText("This application does not support the game you have choosen! Please choose ConquerCorners to play.");
					}
				}
			}
		});
		 
		endPanel.add(tools, BorderLayout.PAGE_START);
		
		endPanel.add(EastPanel,BorderLayout.EAST);
		endPanel.add(MessagePanel, BorderLayout.SOUTH);
		endPanel.add(gamePanel,BorderLayout.CENTER);
		
		JFrame f= new JFrame("Conquer Corners");
		f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
            	C_subject.askServer("Disconnect");
            	C_subject.endClient();
            	 System.exit(0);
            }
        });
		f.getContentPane().add(endPanel);
		f.setPreferredSize(new Dimension(600, 400));
		f.pack();
		f.setLocationRelativeTo(null);
		f.setLocation(300, 150);
		f.setVisible(true);
	}
	private void reloadPlayerPanelContent(){
		PInfoP.removeAll();
		for (Player p : C_subject.getPlayersList()){//TODO: load from Controller
			if(firstLoad){
				p.setColor(gb.getRandomColor()); //TODO: refactor work to Controller
				this.firstLoad=false;
			}
			Color c = p.getColor();
			JLabel nJL=new JLabel();
			nJL.setBackground(c);
			if(p.isMe()){
				nJL.setText("My player; #"+p.getId());
			}else{
				nJL.setText("Other player; #"+p.getId());
			}
			nJL.setBorder(BorderFactory.createLineBorder(c));
			nJL.setForeground(c);
			PInfoP.add(nJL);
		}
		playersPanel.setVisible(true);
		playersPanel.repaint();
		playersPanel.revalidate();
	}
	private void repaintGameBoard(){		
		updateGamePnl(true);
//		System.out.println("Repaint: Is event dispatch thread? "+SwingUtilities.isEventDispatchThread());
		
		gamePanel.repaint();
		gamePanel.revalidate();
	}
	//Public:
	public int getIndex() {

		return observerNr;
	}
		
	public void update(Observable o, Object msg) {
		if(o instanceof Controler){
			this.C_subject=(Controler) o;
			if(msg instanceof String){
				String[] cmd = ((String)msg).split(":");
				switch(cmd[0]){
				case ("newGame"):
					
					joinBtn.setText("Start Game");
					System.out.println("init gb gui");
					gb.initGameBoardGUI(); //TODO: refactor work to Controller
					msgLbl.setText("You have joined a new game of CONQUER CORNERS! The Game will start shortly!");//Shortly
					reloadPlayerPanelContent();
					break;
				case ("NewPl"):
					gb.initColorForPlayers(C_subject.getPlayersList());
					msgLbl.setText("One or more players have joined the game!");
					reloadPlayerPanelContent();
					break;
				case ("GameStart"):
					System.out.println("init gb gui");
					msgLbl.setText("Game has started!");
					reloadPlayerPanelContent();
					this.joinBtn.setVisible(false);
					break;
				case ("move"): 
					gb.updateGameBoard(C_subject.getPlayersList(),C_subject.getSelectedVBs(),C_subject.getCurrentVB(),true);
					repaintGameBoard();
					break;
				case ("conqVB"):
					
					this.gb.copyVBs(C_subject.getSelectedVBs());
//					msgLbl.setText("A corner-box was conquered!");
					gb.updateGameBoard(C_subject.getPlayersList(),C_subject.getSelectedVBs(),C_subject.getCurrentVB(),false);
					repaintGameBoard();
					break;
				case ("newVB"):
//					System.out.println("VB is selected:"+ C_subject.getCurrentVB().isSelcted());

	
//					msgLbl.setText("A new corner-box was found!");
					gb.updateGameBoard(C_subject.getPlayersList(),C_subject.getSelectedVBs(),C_subject.getCurrentVB(),true);
					repaintGameBoard();
					break;
				case ("Game finished!"):
					this.haveJoined=false;
					if(cmd[1].matches("Draw")){
						String[] drawers = cmd[2].split(",");
						String drawMsg="";
						for(String drawer:drawers){
							if(drawers[drawers.length-1].matches(drawer)){
								drawMsg=drawMsg+drawer;
							}else{
								drawMsg=drawMsg+drawer+"&";
							}
						}
						msgLbl.setText("The game ended in a draw between the players [#]: "+drawMsg);
					}else{
						msgLbl.setText(cmd[1]+" is player #"+cmd[2]);
					}
					
					gb=new GameBoard(C_subject.getGameSize(), this.C_subject);
					
					reloadPlayerPanelContent();
					joinBtn.setText("New Game");
					joinBtn.setVisible(true);
					break;
				default : 
					break;
				}
			}
		}
	}
	@Override
	public void run() {
		writeGUI();
		
	}
	

//	private synchronized void setChanged(boolean b){
//	System.out.println("Changing");
//	//		this.hasChanged=b;
//}
	/**
	 * start this thread when game start! 
	 */
	//	public JPanel paintGamePanel(){
	//		JPanel GP=new JPanel();
	//		GP.setLayout(new BoxLayout(GP,BoxLayout.PAGE_AXIS));
	//	}

	//	public void setProp(int x, String str){
	//		switch (x){
	//		case 1:
	//			this.movedir=str;
	//			break;
	//		case 2:
	//			this.newAction=str;
	//			break;
	//		}
	//	}
}
