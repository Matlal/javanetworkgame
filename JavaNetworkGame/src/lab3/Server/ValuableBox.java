package lab3.Server;

public class ValuableBox {

	private boolean selcted;
	private int posY,posX;
	private int id;
	public ValuableBox(int ID,int x, int y){
		this.id=ID;
		this.posX=x;
		this.posY=y;
		this.selcted=false;
	}
	public int getID() {
		return this.id;
	}
	public int getY(){
		return this.posY;
	}
	public int getX(){
		return this.posX;
	}
	public boolean isSelcted(){
		return this.selcted;
	}
	public void setSelcted(){
		this.selcted=true;
	}
	
}

