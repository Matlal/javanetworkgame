package lab3.Server;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;

public class Controller extends Observable{

	/*VARIABLES*/
	private List<Observer> observers;
	private int DEFAULT_SIZE = 8;
	private Server s;
	private Game gm;
	private String message;
	private boolean changed;
	private UDP_Server UDP_Channel;

	/*Constructor*/
	Controller(Server S, UDP_Server udpMini){
		observers = new ArrayList<Observer>();
		this.s=S;
		this.UDP_Channel=udpMini;
		//		t= new Timer();
		gm= new Game(DEFAULT_SIZE, this);
	}

	/*Game Control*/

	//	public MiniServer getPlayerindexfornextTurn(int plNR){
	//		return gm.getPlayerList().get(plNR);
	//	}
	/**
	 * TODO: Control that the incoming pos is correct, if the action is "select"!!!!!!
	 * @param p
	 * @param dir
	 * @return
	 */
	public boolean checkMove(Player p, String dir) {
		if(gm.isMoveOK(dir, p)){
			notifyAllClientsOnUDP("Move:"
					.concat(Integer.toString(p.getID())).concat(":")
					.concat(dir));
			return true;
		}return false;
	}

	public void sendVBtaken(int id, ValuableBox vb) {
		String x= Integer.toString(vb.getX());
		String y= Integer.toString(vb.getY());
		
		notifyAllClients("VBselected:"+id+":"+x+","+y);
	}
	public void sendNewVB(ValuableBox vb) {
		String x= Integer.toString(vb.getX());
		String y= Integer.toString(vb.getY());
		
		notifyAllClients("newValuableBox:".concat(x).concat(",").concat(y));
		
	}
	public void sendGameEnd( ){
		/**
		 * TODO: Fix, in UDP implementations, so  "game finished"- message runs over UDP. Otherwise chances are that the client keeps sending MoveReqs!
		 */
		String msgData="";
		if(gm.getWnners().size()==1){
			msgData=msgData.concat(Integer.toString(gm.getWnners().get(0).getID())); 
			notifyAllClients("Game finished!:Winner:".concat(msgData));
		}else{
			for(Player p: gm.getWnners()){
				if(gm.getWnners().size()-1==gm.getWnners().indexOf(p)){
					msgData=msgData.concat(Integer.toString(p.getID()));
				}else {
					msgData=msgData.concat(Integer.toString(p.getID()).concat(","));
				}
			}
			notifyAllClients("Game finished!:Draw:"+msgData);
		}
		gm= new Game(DEFAULT_SIZE, this);
		
	}

	public void sendGameInfo(){
		String msgData="";
		String id= "";
		String x= "";
		String y= "";
		for(int i=0; i< gm.getPlayerList().size();i++){
			id= Integer.toString(gm.getPlayerList().get(i).getID());
			x= Integer.toString(gm.getPlayerList().get(i).getX());
			y= Integer.toString(gm.getPlayerList().get(i).getY());
			if(i!= gm.getPlayerList().size()-1) {
				msgData=msgData+id+":"+x+","+y+":";
			}else {
				msgData=msgData+id+":"+x+","+y;
			}
		}

		//		GameInfo:size:player#:player pos [x,y]:
		notifyAllClients("GameInfo:".concat(msgData));
	}


	public int getAmountOfPlayers(){
		return gm.getPlayerAmount();
	}

	/*Server Control*/

	
	public void parseMsg(String msg, MiniServer miniS){
		System.out.println("Client nr "+miniS.getID()+">" + msg);
		String[] strList= msg.split(":", -1);
		switch(strList[0]){
		case "Disconnect": 
			try {
				miniS.disconnect();
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;		
		case "LeaveReq": 
			miniS.joinedGame=false;
			break;		
		case "JoinReq": 
			if((gm.getPlayerList().indexOf(miniS)==-1) && miniS.joinedGame==false) {
				if(gm.canJoin(miniS)){
					miniS.joinedGame=true;
					this.notifyOneClient(miniS.getID(), "Player ID:"+miniS.getID());
					sendGameInfo();
					System.out.println("client #"+miniS.getID() + " has joined the game as player #"+miniS.getID());
					miniS.sendMessage("newUDPport:"+miniS.grpAddr+":"+Integer.toString(miniS.udpPort));
				}
				else{
					miniS.joinedGame=false;
					miniS.sendMessage("Game has started. Retry in a few minutes.");
				}
			}else {
				System.out.println("client #"+miniS.getID()+" is already playing");
			}
			break;
		case "StartReq": 
			gm.start();
			//			String tO=getTurnOrder();
			//			System.out.println("Game Start! ");
			//			clientList.get(turnIndex).setMyTurn(true);
			
			notifyAllClients("Game start!");
			ValuableBox currVB=gm.getCurrentVB();
			String VB_x=Integer.toString(currVB.getX());
			String VB_y=Integer.toString(currVB.getY());
			notifyAllClients("newValuableBox:"+VB_x+","+VB_y);
			break;
		case "MoveReq":
			//			if(miniS.isMyTurn()==true){
			checkMove(miniS,strList[1]); //TODO: make this a UDP Action
			//			}
			//			else{
			//				miniS.sendMessage("Not your turn");
			//			}
			break;
			//		case "Close game start-ACK":
			//			miniS.hasACKedNewGame=true;
			//			break;

		default : System.out.println("Not handled!");
		/**
		 * TODO:update observable
		 */
		break;
		}
	}
	public void attach(Observer observer){
		observers.add(observer);
	}
	public void deattach(Observer observer){
		observers.remove(observer);
	}
	public void notifyAllClients(Object obj){
		boolean changeState=false;
		if(obj instanceof String){
			String[] cmd = ((String) obj).split(":");
			if(cmd[0].matches("Game finished!")){
				changeState=true;
			}
		}
		for (Observer observer : observers) {
			//			System.out.println("Observer nr "+observer.getIndex()+"exist!");
		
			if(observer.hasJoinedGame()){
				if(changeState){
					observer.kickOutPlayer();
				}
				observer.update(this, obj);
			}
		}
	} 
	public void notifyAllClientsOnUDP(String msg){
		this.UDP_Channel.sendUDPMessage(msg);
	}
	public void notifyOneClient( int nr,Object obj){
		try {
			Player miniS= gm.getPlayerList().get(nr-1);
			miniS.update(this, obj);
		} catch (IndexOutOfBoundsException indexE) {
			// TODO Auto-generated catch block
			indexE.printStackTrace();
		}
	}
	public int getAmountOfObservers(){
		return this.observers.size();
	}
	/**
	 * 
	 * @param index	= the number of the associated client minus [i] - 1
	 * @param message
	 * @throws IndexOutOfBoundsException
	 */
	//	private Timer t ;
	//	private long newGameTimerInterval= 1000*11*1;	// 1 minute!!!
	//	private long ACK_Interval=1000*1;		//1 second
	//	private long timerStartTime;
	//	public void RejectUnACKedClients(){
	//		for(MiniServer client : clientList){
	//			if(!client.hasACKed()){
	//				notifyOneClient( client,"Unresponsive client");
	//				clientList.remove(clientList.indexOf(client)); //OBS : Could be dangerous to delete here !!!
	//			}
	//		}
	//	}
	//	
	//	public void addClient(MiniServer s){
	//	clientList.add(s);
	//}


	//public void removeClient(MiniServer s){
	//	clientList.remove(s);
	//}
	//	public MiniServer getClientWhoHasNextTurn(){
	//	if(turnIndex==clientList.size()){
	//		turnIndex=0;
	//	}
	//	return clientList.get(turnIndex);
	//}
	//public String getTurnOrder(){
	//	String turnOrder="";
	//	for(int i=0;i< clientList.size();i++){
	//		turnOrder=turnOrder.concat(Integer.toString(clientList.get(i).getPlayerIndex()).concat(":"));
	//	}
	//	return turnOrder;
	//}
	//	public int getAmountOfClients() {
	//		return this.clientList.size();
	//	}
	//	public void startTimerForClientToACK(){
	//		t.schedule(new TimerTask(){
	//			@Override
	//			public void run() {
	//				RejectUnACKedClients();
	//				gm.start();
	//				Integer x =null; 
	//				String tO=getTurnOrder();
	//				System.out.println("Waiting on action from player nr: "+getClientWhoHasNextTurn().getPlayerIndex());
	//				clientList.get(turnIndex).setMyTurn(true);
	//				notifyAllClients("Game start:Turn-order:".concat(tO));
	//			}}
	//		, ACK_Interval);
	//	}

	//	public int getState() {
	//	return state;
	//}
	//	public void notifyAllObservers(Object obj){
	//		for (Observer observer : observers) {
	//			//			System.out.println("Observer nr "+observer.getIndex()+"exist!");
	//			if(observer.hasJoinedGame()){
	//				observer.update(this, obj);
	//			}
	//
	//		}
	//	} 
	//	public void setState(Object obj) {
	//	this.state = state;
	//	//notifyAllObservers(obj);
	//}
	//	public void notifyOneObserver( int CliNr,Object obj){
	//		for (Observer observer : observers) {
	//			if(observer.getPlayerIndex()==CliNr){
	//				observer.update(this, obj);
	//				if (observers.indexOf(observer)!=observers.size()-1) {
	//					observers.get(observers.indexOf(observer) + 1).update(this,obj);
	//				}else{
	//					observers.get(0).update(this,obj);
	//				}
	//			}
	//		}
	//	} 

	//	public void startTimerForNewGame(){
	//	timerStartTime=System.currentTimeMillis();
	//	t.schedule(new TimerTask(){
	//		@Override
	//		public void run() {
	//			if(!clientList.isEmpty()){
	//				String msgData="";
	//				ValuableBox vb = getNewVB();
	//				String x= Integer.toString(vb.getX());
	//				String y= Integer.toString(vb.getY());
	//				notifyAllClients("newValuableBox:".concat(x).concat(",").concat(y));
	//				startTimerForClientToACK();
	//			}
	//			else{
	//				/**
	//				 * TODO: do something here to get the server to sleep (e.g. wait for joinReq)
	//				 */
	//			}
	//		}}
	//	, newGameTimerInterval);
	//}

	//	public void updateOneClient(int ClientNr, String msg){
	//		sendToOne(ClientNr-1, msg);
	//	}
	//	public void updateAllClient( String msg){
	//		sendToAll(msg);
	//	}


	//	public void update(Observable o, Object arg) {
	//		/**
	//		 * TODO: Create functional update
	//		 */
	//		switch (arg) {
	//		case GomokuClient.CLIENT:
	//			message = "Game started, it is your turn!";
	//			currentState = MY_TURN;
	//			break;
	//		case GomokuClient.SERVER:
	//			message = "Game started, waiting for other player...";
	//			currentState = OTHER_TURN;
	//			break;
	//		}
	//
	//		refresh();
	//	}



}