package lab3.Server;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;

public class Game extends Observable{

	private int size;
	private int innerStartRadius=0;
	private int grid[][];
	private boolean started=false;
	private ArrayList<Player> players= new ArrayList<Player>();
	private ArrayList<ValuableBox> VBoxes= new ArrayList<ValuableBox>();
	private ValuableBox currentVB;
	private Controller ctrl;
	/**
	 * Constructor
	 * 
	 * @param size
	 *            The width/height of the game grid
	 */
	public Game(int Size, Controller c) {
		this.ctrl=c;
		this.size = Size;
		grid = new int[size][size];
		this.players.clear();
		initGrid();
		initValuableBoxes();
	}
	public void initGrid(){
		for (int a = 0; a < size; a++) {
			for (int i = 0; i < size; i++) {
				grid[a][i] = 0;
			}
		}
	}
	public void initValuableBoxes(){
		this.VBoxes.clear();
		VBoxes.add(new ValuableBox(0 ,0, 0));
		VBoxes.add(new ValuableBox(1,0, size-1));
		VBoxes.add(new ValuableBox(2, size-1, 0));
		VBoxes.add(new ValuableBox(3, size-1, size-1));
		this.currentVB=getNewVB();
	}
	public void start(){
		this.started=true;
	}
	public boolean hasStarted(){
		return started;
	}
	public void addPlayer(Player p){
			int id=players.size()+1;
			p.setID(id);
			ArrayList<Integer> startPos=getStartXY(p);
			
			p.setPos(startPos.get(0), startPos.get(1));
			players.add(p);
			System.out.println("New player added: #"+p.getID()+" ["+startPos.get(0)+","+ startPos.get(1)+"]" );
	}
	public ArrayList<Integer> getStartXY(Player p){
		if(players.size()>3){
			innerStartRadius=2;
		}else{
			innerStartRadius=1;
		}
		String startXY=Integer.toString(size/2-innerStartRadius)+","+Integer.toString(size/2-innerStartRadius); 
		String endtXY=Integer.toString(size/2-1+innerStartRadius)+","+Integer.toString(size/2-1+innerStartRadius); 
		System.out.println("posible start x/y: "+startXY);
		System.out.println("posible end x/y: "+endtXY);
		ArrayList<Integer> Coords = new ArrayList<Integer>();
		for ( int startY=(size/2)-innerStartRadius;startY < ((size/2)+innerStartRadius); startY++) {
			for (int startX=(size/2)-innerStartRadius;startX < ((size/2)+innerStartRadius); startX++) {
				//System.out.println("first :"+startY + ","+startX+" has the value: "+grid[startY][startX]);
				if(grid[startY][startX]==0){
					grid[startY][startX]=p.getID();
					Coords.add(startX);
					Coords.add(startY);
//					System.out.println("player has the pos of: "+Coords.get(0)+","+Coords.get(1));
					return Coords;
				}
			}
		}
		return null;
	}


	public void checkifVB(int y, int x, Player p) {
		if(currentVB.getX()==x && currentVB.getY()==y){
			VBoxes.get(currentVB.getID()).setSelcted();
			p.addVB();
			ctrl.sendVBtaken(p.getID(),currentVB);
			if(!checkVBLeft()){
				ctrl.sendGameEnd();
			}else {
				this.currentVB=getNewVB();
				ctrl.sendNewVB(currentVB);
			}
		}
	}
	public ValuableBox getCurrentVB() {
		return this.currentVB;
	}
	private ValuableBox getNewVB(){
		while(true){
			int randomNum = ThreadLocalRandom.current().nextInt(0, 3 + 1);
			if(!(VBoxes.get(randomNum).isSelcted())){
				return VBoxes.get(randomNum);
			}
		}
	}
	public boolean moveplayer(int x, int y, Player p){
		if (grid[y][x] == 0) {
			grid[y][x] = p.getID();
			p.setNewPos(x,y);
			System.out.println("Gridvalue at new pos: "+x+","+y+" is: "+grid[y][x]);
			checkifVB(y,x, p);
			return true;
		}
		return false;
	}
	public boolean isMoveOK(String dir, Player pl) {
		if(this.started){
		Player p=players.get(pl.getID()-1);
		if(p!=null){
			int y=p.getY();
			int x=p.getX();
			switch(dir){
			case("South"):
				if((y+1)<size && moveplayer(x,y+1, p)){
					grid[y][x] = 0;
					return true;
				}
			break;
			case("West"):
				if((x-1)>(-1) && moveplayer(x-1,y, p)){
					grid[y][x] = 0;
					return true;
				}
			break;
			case("East"):
				if((x+1)<size && moveplayer(x+1, y, p)){
					grid[y][x] = 0;
					return true;
				}
			break;
			case("North"):
				if((y-1)>(-1) && moveplayer(x,y-1, p)){
					grid[y][x] = 0;
					return true;
				}
			break;
			}
		}
		}
		return false;
	}
	/**
	 * right now: unnecessary
	 */
	public void addNewVB(){

	}
	public Player getPlayer(int PlayerNR){
		for(Player p: players){
			if(p.getID()==PlayerNR)
				return p;
		}
		return null;
	}
	public ArrayList<Player> getWnners(){
		ArrayList<Player> winners=new ArrayList<Player>();
		int highestVBcount=0;
		for(Player p: players){
			if(p.getVBs()>highestVBcount){
				highestVBcount=p.getVBs();
			}
		}
		for(Player p : players){
			if(p.getVBs()==highestVBcount){
				winners.add(p);
			}
		}
		return winners;
	}

	public ArrayList<Player> getPlayerList(){
		return players;
	}
	public ArrayList<ValuableBox> getValuableBoxesList(){
		return VBoxes;
	}

	public int getPlayerAmount(){
		return players.size();
	}
	/**
	 * To extract coordinates, use culmNr || x = int[0], rowNr || y=int[1]. 
	 * @param pNr
	 * @return
	 */
	public int getPlayerPos(int pNr){
		for (int a = 0; a < size; a++) {
			for (int i = 0; i < size; i++) {
				if(grid[a][i] == pNr){
					return grid[a][i];
				}
			}
		}
		return -1;
	}

	/**
	 * Reads a location of the grid
	 * 
	 * @param x
	 *            The x coordinate
	 * @param y
	 *            The y coordinate
	 * @return the value of the specified location
	 */
	public int getLocation(int x, int y) {
		return grid[y][x];
	}

	/**
	 * Returns the size of the grid
	 * 
	 * @return the grid size
	 */
	public int getSize() {
		return size;
	}
	/**
	 * Clears the grid of pieces
	 */
	public void clearGrid() {
		for (int a = 0; a < size; a++) {
			for (int i = 0; i < size; i++) {
				grid[a][i] = 0;
			}
		}
		setChanged();
		notifyObservers();
	}
	public boolean canJoin(MiniServer miniS) {
		double maxPlayers=Math.pow(4, innerStartRadius);
		if((!started) && (players.size()<maxPlayers)){
			
			addPlayer(miniS);
			return true;
		}
		return false;
	}
	public boolean checkVBLeft() {
		for(ValuableBox VB:VBoxes){
			if(!VB.isSelcted()){

				return true;
			}
		}
		started=false;
		
		
		return false;
	}

	public void removeplayers(){
		players.clear();
	}
	public void removeplayer(MiniServer miniS){
		players.remove(miniS);
	}

//	/**
	//	 * TODO: Security issue: player can select box without beeing on the corresponding coords! 
	//	 * @param pos
	//	 * @param clientNr
	//	 * @return
	//	 */
	//	public boolean isSelectBoxOK(String[] pos, int clientNr) {
	//		Player p = getPlayer(clientNr);
	//		int posX=Integer.parseInt(pos[0]);
	//		int posY=Integer.parseInt(pos[1]);
	//		for(ValuableBox VB:VBoxes){
	//			if((VB.getX()==(posX))&&(VB.getY()==posY)){
	//				if(!VB.isSelcted()){
	//					VB.setSelcted();
	//					p.addVB();
	//					return true;
	//				}
	//			}
	//		}
	//		return false;
	//	}
}
