package lab3.Server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Observable;

public class UDP_Server  {

	private MulticastSocket UDP_socket;
	private  DatagramPacket packet;
	private  ArrayList<String> msgList = new  ArrayList<String>();
	final protected  char[] hexArray = "0123456789ABCDEF".toCharArray();
	private InetAddress IPAddress;
	private int UDPport; 
	int observerNr=0;
	byte[] buf = new byte[256];
	private int clientNr;
	private MiniServer miniS;

	public UDP_Server( MulticastSocket UDP_socketIN, int UDPport1, InetAddress ipA){
		this.UDP_socket=UDP_socketIN;
//		UDP_socket.set
		this.UDPport=UDPport1;
//		this.miniS=miniServer;
		this.IPAddress=ipA;
//		this.clientNr=cliNr;
//		try {
//			UDP_socket.setReuseAddress(true);
//		} catch (SocketException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	/*public void run() {
//			UDP_socket.setReuseAddress(true);
		System.out.println("while starts");

			while (true){
				byte[] buffer = new byte[1024];
				//read
				packet=new DatagramPacket(buffer,buffer.length);
				
				int port = packet.getPort();
				InetAddress adr= packet.getAddress();
				 if(this.UDPport!=port){
					 this.UDPport=port;
				 }
				 if(this.IPAddress!=adr){
					 this.IPAddress=adr;
				 }
				System.out.println("host adress: "+IPAddress.getHostAddress());
				System.out.println("IPAddress: "+IPAddress.toString());
//				byte[] newbuffer= new byte[packet.getLength()];
//				System.out.println("new UDP msg read");
//				for(int i =0;i<packet.getLength();i++){
//					newbuffer[i]=buffer[i];
//				}
//				System.out.println(" UDP from "+packet.getAddress()+" "+bytesToHex(newbuffer)+" "+new java.util.Date());
				String received = new String(packet.getData(), 0, packet.getLength());
			     
			    parseMsg(received);
			}
			UDP_socket.close();

	}
	public void parseMsg(String msg){
				System.out.println("Client>" + msg);
				String[] strList= msg.split(":", -1);
				switch(strList[0]){
				case "helloUDP": 
					sendUDPMessage("hello");
					
					break;
				case "ActionReq":
					if(miniS.isMyTurn()==true){
						miniS.getController().isActionOk(miniS.getPlayerIndex(),strList[1],strList[2]);
					}
					else{
						miniS.sendMessage("Not your turn");
						
					}
					break;
				} catch (IOException e) {
					e.printStackTrace();
				}
				c.deattach(this);
				c.removeClient(this);
				break;		
				case "LeaveReq": 
					this.joinedGame=false;
				break;		
				case "JoinReq": 
					this.joinedGame=true;
					if(c.canJoinGame(this)){
						 	// XXX: correct ?
					
					}
					else{
						this.joinedGame=false;
						sendMessage("Game started. Retry in a few minutes.");
					}  
					break;
				case "Close game start-ACK":
					this.hasACKedNewGame=true;
					break;
				case "ActionReq":
					if(this.myTurn==true){
						c.isActionOk(instNr,strList[1],strList[2]);
					}
					else{
						sendMessage("Not your turn");
					
					}
					break;
				default : System.out.println("Not handled!");
			
				break;
				}

	}*/
	public String bytesToHex(byte[] bytes) {   //convert byte[] to string
		char[] hexChars = new char[bytes.length * 2];
		for ( int j = 0; j < bytes.length; j++ ) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
	public byte[] hexStringToByteArray(String s) {               //Convert string to byte[]
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
					+ Character.digit(s.charAt(i+1), 16));
		}
		return data;
	}


	public void sendUDPMessage(String msg) {
		System.out.println("--Server on UDP Multicast channel> "+ msg);
		try{ // send request
        buf=msg.getBytes();
//		IPAddress= InetAddress.getByAddress(addr);
		DatagramPacket packet = new DatagramPacket(buf, buf.length, IPAddress, UDPport);
        UDP_socket.send(packet);
        
	}catch(IOException ioE){
		ioE.printStackTrace();
	}
	}

	public int getPortnr(){
		return this.UDPport;
	}
	public int getClientIndex() {
		return this.clientNr;
	}

	public UDP_Server getUDPChannel() {
		// TODO Auto-generated method stub
		return this;
	}



}
