package lab3.Server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Vector;

public class MiniServer extends Player implements Runnable{

	protected Socket socket;
	Controller serverCtrl;
	DataInputStream in = null;
	DataOutputStream out = null;
	private String message;
//	int cliNr;	//Client index = instNr-1
//	int playerNR;
	int udpPort;
	boolean joinedGame=false;
//	boolean hasACKedNewGame=false; 
//	boolean myTurn=false; 
	boolean running=true;
	String grpAddr;
	
	MiniServer( Controller serverCtrl, Socket socket, int udpPort1, String groupaddr) {
		this.udpPort=udpPort1;
		this.socket = socket;
//		this.cliNr=nrID;
		this.serverCtrl=serverCtrl;
		serverCtrl.attach(this);
		this.grpAddr=groupaddr;
	}
	public void initConn() throws IOException{
		out =new DataOutputStream(socket.getOutputStream());
		in =new DataInputStream(socket.getInputStream());
		sendMessage("Connection successful");
		//		   sendMessage("newUDPport:".concat(Integer.toString(this.UDPPort)));
	}
	public void run(){
		try{
			
			//3. get Input and Output streams
			initConn();
			System.out.println("MiniServer>UDP and Tcp connections are running");
			//4. The two parts communicate via the input and output streams
			while(running){
					String message= in.readUTF();
					serverCtrl.parseMsg(message, this);
			}
			
		}catch(SocketException SocE){
			SocE.printStackTrace();
			System.err.println("MiniServer>Server lost the connection to client nr: "+this.getID());
		}catch(IOException ioException){
			ioException.printStackTrace();
		}finally {
			destroyThread();
		}
		//Read input and process here
	}
	void sendMessage(String msg)
	{
		try{
			out.writeUTF(msg);
			out.flush();

			System.out.println("--MiniServer to client #"+this.getID()+">" + msg);
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}
	public void setFlagJoinedGame(boolean b){
		this.joinedGame=b;
	}
	public boolean hasJoinedGame(){
		return this.joinedGame;
	}
//	public boolean hasACKed(){
//		return hasACKedNewGame;
//	}
//	public int getClientIndex(){
//		return this.cliNr;
//	}
//	public void setMyTurn(boolean b){
//		this.myTurn=b;
//	}
//	public boolean isMyTurn(){
//		return this.myTurn;
//	}
	public void update(Observable o, Object arg) {
		this.serverCtrl=(Controller) o;
		if(arg instanceof String){
			//			String cmd=((String) arg).split(":")[0];
			//			if(cmd.matches("ActionUpdate")){
			//				this.udpMini.sendUDPMessage((String)arg);
			//			}else{
			sendMessage( (String) arg);
		}
		if(arg instanceof Integer){
//			setMyTurn(!(this.myTurn));
		}
	}
	public void destroyThread() {
		if (Thread.currentThread()!= null) {
			
			try {
				Thread.currentThread().join(400);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public void disconnect()throws IOException{
		running=false;
		System.out.println("Connection with client nr "+this.getID()+" is now closed.");
		//		c.deattach(this);
		socket.shutdownInput();
		socket.close(); 
	}
//	public int getPlayerIndex() {
//		return this.playerNR;
//	}
	//	public UDP_Server getUDPChannel() {
	//		// TODO Auto-generated method stub
	//		return udpMini;
	//	}
	@Override
	public void kickOutPlayer() {
		// TODO Auto-generated method stub
		this.joinedGame=false;
	}
}


