package lab3.Server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import serviceDiscovery.ServiceDescription;
import serviceDiscovery.ServiceResponder;

public class Server {

	/**
	 * TODO: CURRENT STATE:
	 * 			-when client leaves => clientlist = 0 !!! 
	 */
	Controller serverCtrl;
	private  ArrayList<Integer> PortBuffer = new  ArrayList<Integer>();

	private static final int port = 6679;
//	private static final int UDPport = 1900;
	private static final String groupAddr="239.255.255.251";
	private static InetAddress serverAddr;
	private InetAddress ipA;
	int udpPort;
	private Socket clientSocket=null;
	private ServerSocket serverSocket=null;
	byte[] buf = new byte[100000];
	private UDP_Server udpMini;
	private int startingportnr= 6667;
	private String SERVICE_NAME  = "JavaGameServer";
	private String INSTANCE_NAME = "ConquerCorners_by_Matlal";
	
	/**
	 * --------------------------------Start------------------------
	 * @param args
	 * @throws IOException
	 */
	public static void main(String [] args)throws IOException{
		Server s= new Server();
	}

	public Server(){
		udpPort=getfreePort();
		try {
			serverSocket = new ServerSocket();
			
			serverAddr=InetAddress.getLocalHost();
			ipA=InetAddress.getByName(groupAddr);
			serverSocket.bind( new InetSocketAddress(serverAddr, 0));    /*bind to any available port number*/
			MulticastSocket UDP_socket = new MulticastSocket();
			udpMini=new UDP_Server(UDP_socket,udpPort,ipA);
		}
		catch (IOException ioe) {
			System.err.println( "Could not bind a server socket to a free port: "+ioe);
			System.exit(1);
		}
		
		createServiceDiscovery();
		
		serverCtrl= new Controller(this, udpMini);

		boolean listeningSocket = true;
//		int i= 0;
//		 As long the server is active, it tries to creates new threads for new clients after established connection with the last client, all on the server socket.
		System.out.println( "Responder listening. Now taking connections...");
		while(listeningSocket){
			
//			i= serverCtrl.getAmountOfObservers()+1;
			System.out.println("SERVER: Waiting for new connection");

			try {
				clientSocket = serverSocket.accept();
				
				
				System.out.println("Connection received from " + serverSocket.getInetAddress().getHostAddress()+ ", at local port:" +serverSocket.getLocalPort());
				//				int udpPort=getfreePort();
				MiniServer new_MiniS=new MiniServer(serverCtrl,clientSocket, udpPort, groupAddr);
				new Thread(new_MiniS).start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				break;
			}
		}
		try {
			serverSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	private int getfreePort() {
		boolean foundPort=false;
		int x=startingportnr;
		if(PortBuffer.size()==0){
			PortBuffer.add(startingportnr);
		}else{
			while(x<(startingportnr+100)){
				for(Integer j:PortBuffer){
					if(x!=j){
						foundPort=true;
						PortBuffer.add(x);
						break;
					}
				}
				if(foundPort){
					break;
				}else {
					x++;
				}
			}
			if(!foundPort){
				System.out.println("more than 100 clients exist!");
			}
		}
		return x;
	}
	
	private void createServiceDiscovery() {
		/* Create a descriptor for the service you are providing. */
		ServiceDescription descriptor = new ServiceDescription();
		descriptor.setAddress(serverSocket.getInetAddress());
		descriptor.setPort(serverSocket.getLocalPort());
		descriptor.setInstanceName(INSTANCE_NAME);
		System.out.println("Service details: "+descriptor);

//		  Set up a responder and give it the descriptor (above) we want to publish. Start the responder, which works in its own thread.
		 
		ServiceResponder responder = new ServiceResponder(SERVICE_NAME);
		responder.setDescriptor(descriptor);
		responder.startResponder();
	}
}








