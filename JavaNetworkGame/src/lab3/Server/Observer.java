package lab3.Server;

import java.util.Observable;


public abstract class Observer {
		   protected Controller C_subject;
		   public abstract void update(Observable o, Object msg) ;
//		   public abstract int getClientIndex() ;
//		   public abstract UDP_Server getUDPChannel();
		   public abstract boolean hasJoinedGame() ;
		   public abstract void kickOutPlayer() ;
}

