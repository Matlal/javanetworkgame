package lab3.Server;

import java.util.ArrayList;

public abstract class Player extends Observer {

	private int id;
	private int nrOfVB=0;
	private int x, y;
	
	public void setPos(int X, int Y){
		
		this.x=X;
		this.y=Y;
	}
	public void setID(int ID){
		this.id=ID;
	}
	
	public int getY(){
		return y;
	}
	public int getX(){
		return x;
	}
	public int getID(){
		return id;
	}
	public void setNewPos(int newX, int newY ){
			this.x=newX;
			this.y=newY;
	}
	public void addVB(){
		nrOfVB++;
	}
	public int getVBs(){
		return nrOfVB;
	}
	
	
}
