package serviceDiscovery;

public interface ServiceBrowserListener {
	public abstract void serviceReply(ServiceDescription descriptor);
}
