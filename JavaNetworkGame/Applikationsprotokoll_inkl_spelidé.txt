﻿Dokumentation för Laboration 3: Enkelt java spel

Spelidé:
Mitt spel handlar om att spelare ska ’fånga’/ ta vilktiga positioner(hörn) på ett fyrkantigt spelplan som är uppdelat i 8x8 rutor/celler. 

All bedömning i spelet bestäms av servern. 

En eller flera klienter agerar som spelare (’player’) när de har anslutit till servern och tryckt för att delta i ett nytt spel.  Spelaren/-na börjar i mitten av spelplanen.
När någon utav spelarna tryckt igång spelet, bestämmer servern spelstart och meddelar alla spelare detta (även om det bara är en enda). 

Servern hanterar föflytningsförfrågningar av klienterna och tilldelar ett ja till när den har blivit godkänd.
En spelare kan inte gå utanför seplplanen eller okuppera en redan okupperad cell/ruta.  

När en ruta som värderas som en viktig position (’ValuableBox’) fångas, registreras detta och servern meddelar alla detta och skickar ut vilken ruta (vilket hörn) som skall fångas härnäst. 

När all rutorna är fångade/tagna, slutar spelet och en vinnare utses. 

Nätverksprotokollet som används är följande:
1. ’JoinReq’
2. ’Player ID:#’[id]
3. ’GameInfo:[#:#,#]*(x)' [id:x-coord,y-coord]*(antal spelare)
4. ’StartReq’
5. ’Game start!’
6. ’newValuableBox:#,#’[x-coord,y-coord]
7. ’MoveReq:dir’[vädersträck]
8. ’Move:#:dir’ [id:vädersträck]
9. 'VBselected:#:#,#' [id:x-coord,y-coord]
10. ’Game finished!:Winner/Draw:#/#,#,#’[id]/[id]*(antal vinnare)
11. 'Disconnect'

Förklaring:
1. En klient ber om att få ansluta med ’JoinReq’
2. Om klienten får vara med i spelet, svarar Servern med ’Player ID:#’. Om Servern inte svarar betyder det att spelet har för många spelare eller redan startat.
3. ’GameInfo’ skickas ut till alla spelare varje gång en ny spelare tillkommer i spelet (innan start). Meddelandet innehåller alla spelares id och deras respektive startposition. Ex, ’GameInfo:1:3,3:2:4,3’ betyder två spelare som har positionerna 3,3 respektive 4,3. 
4. ’StartReq’ skickas när en spelare/klient vill starta spelet. Värt att notera är att detta meddelande kan teoretiskt sett inkomma till servern fler än 1 gång, om fler en 1 klienter ber om spelstart inom en tätt intervall (nästan samtidigt). Spelarna begränsas denna möjlighet så fort nästa meddelande (5:’Game start!’) inkommer till klienten. 
5. ’Game start!’ skickas till alla spelare när den första ’StartReq’ har registrerats hos servern. 
6. ’newValuableBox’ med tillagd data skickas till alla klienter vid två tillfällen; när spelet precis har börjat och när en spelare har fångat en nuvarande ValuableBox. Datat beskriver var nästa Valuablebox har lagts.
7. ’MoveReq’ adderat med ett vädersträck skickas varje gång spelaren vill flytta sin markör åt det aktuella hållet (alltså styrning via piltangenterna). 
8. ’Move’ och medföljande data skickas när en ’MoveReq’ har accepterats av servern. Datat beskriver vilken spelare som har flyttats och åt vilket vädersträck.
9. ’Game finished!:’ skickas när alla hörn(’ValuableBoxes’) har blivit tagna. 
 
Steg 6-8 upprepas tills spelet är slut (tills meddelande #10 sker).

